app.service("auth",["$http","$rootScope",function ($http,$rootScope) {
	$http.defaults.headers.common.Authorization = 'Bearer TOKEN_DEV_CFNeVo71dgF0l4WV8wffK8XcXe5565U1YpfMD9hiCFMmconxMLHUpM';
	return {

		login:function (user,cb) {
			$http.get("https://bourns.white-label-dev.co.uk/api.php?command=list")
				.success(function (users) {
					for(var u of users){
						if(u.usernumber == user.number && u.userpass == user.password ){
							
							if(u.userlevel == "Administrator"){
								return cb({success:true,user:u});
							}
							return cb({success:false,user:null,message:"Only admin allowed"});
						}
					}
					return cb({success:false,user:null,message:"Error in credentials"});
				})
				.error(function () {
					return cb({success:false,user:null,message:"Error happened"});
				});
			
		},
		isLogged:function () {
			try{
				return $rootScope.globals.user != null;
			}catch(e){
				return false;
			}
		}
	};
}]);

app.factory("UserAccount",["$http",function ($http) {
	$http.defaults.headers.common.Authorization = 'Bearer TOKEN_DEV_CFNeVo71dgF0l4WV8wffK8XcXe5565U1YpfMD9hiCFMmconxMLHUpM';
	return {
		getLevels:function (cb) {
			return cb({success:true,levels:["Operator","Supervisor","Administrator"]});// FIXME
			$http.get("url/to/list/roles")
				.success(function (data) {
					cb({success:true,levels:data.levels});
				});
		},
		getUsers:function (user,cb) {

			$http.get("https://bourns.white-label-dev.co.uk/api.php?command=list")
				.success(function (data) {
					console.log(data);
					cb({success:true,accounts:data});//the accounts property is supposed to be an array of accounts objects (having properties:account_id,username,password,level)
				})
				.error(function () {
					cb({success:false,accounts:null,message:"error occured"});
				});
		},
		deleteUser:function (user,cb) {
			$http.get("https://bourns.white-label-dev.co.uk/api.php?command=delete&userid="+user.userid)
				.success(function (d) {
					cb({success:true});
				})
				.error(function () {
					cb({success:false});
				});
		},
		updateUser:function (user,cb) {

			$http.get("https://bourns.white-label-dev.co.uk/api.php?command=update&userid="+user.userid+"&username="+user.username+"&usernumber="+user.usernumber+"&userpass="+user.userpass+"&userlevel="+user.userlevel)
				.success(function (d) {
					cb({success:true});
				})
				.error(function () {
					cb({success:false});
				});
		},
		addUser:function (user,cb) {
			$http.post("https://bourns.white-label-dev.co.uk/api.php?command=create&username="+user.username+"&userpass="+user.userpass+"&userlevel="+user.userlevel+"&usernumber="+user.usernumber)
				.success(function (d) {
					if(d.success){
						cb({success:true,user:user});
					}
				})
				.error(function () {
					cb({success:false,user:user});
				});
		}
	};
}]);

app.service("Util",function () {
	return{
		getIngredients:function () {
			/*
			
			return $http()

			*/
			return [
					  {
					    name:'Acid bath',
					    properties:{duration:'',acidStrength:''},
					  },
					  {
					    name:'Oven bake',
					    properties:{duration:'',temp:''},
					  },
					  {
					    name:'Etch',
					    properties:{depth:''},
					  },
					  {
					    name:'Diffuse',
					    properties:{prop1:''},
					  }
				];
		},
		cloneObject:function(obj) {
		    if (obj === null || typeof obj !== 'object') {
		        return obj;
		    }
		 
		    var temp = obj.constructor(); // give temp the original obj's constructor
		    for (var key in obj) {
		        temp[key] = this.cloneObject(obj[key]);
		    }
		 
		    return temp;
		},
		clearSelected:function() {
		  var ingredients = document.getElementById ("left").getElementsByTagName ("div");
		  for (var i = 0; i < ingredients.length; ++i)
		    ingredients[i].className = ingredients[i].className.replace ("selected", "");
		},
		doProperties:function(item,$scope) {
		    this.clearSelected();
		    item.className = item.className + " selected";
		    document.getElementById ("propname").textContent = item.getElementsByTagName ("b")[0].textContent;
		    var itemIndex = item.getAttribute("index");
		    this.displayProperties(itemIndex,$scope);
		},
		displayProperties:function(index,$scope) {
		  var propertiesContainer = document.getElementById("properties-form");
		  propertiesContainer.innerHTML = "";

		  var properties = $scope.recipeProcess[index].properties;

		  for(var prop in $scope.recipeProcess[index].properties){
		    (function(i) {
		      var form_group = document.createElement("div");
		      var label = document.createElement("label");
		      var input = document.createElement("input");
		          input.addEventListener("change",function() {
		            $scope.recipeProcess[index].properties[i] = input.value;
		          });

		      form_group.className = "form-group";
		      input.className = "form-control";
		      label.innerText = i+":";
		      input.value = $scope.recipeProcess[index].properties[i];
		      form_group.appendChild(label);
		      form_group.appendChild(input);
		      propertiesContainer.appendChild(form_group);
		    })(prop);
		  }
		}
	}
});
