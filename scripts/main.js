var app = angular.module ("app", ["ngRoute","ng-sortable"]);

app.config (["$routeProvider", function ($routeProvider) {
	$routeProvider
		.when ("/login", {
			templateUrl: "templates/login.html",
			controller: "authCtrl",
		})
		.when ("/user/manage", {
			templateUrl: "templates/user_manager.html",
			controller: "userCtrl",
			resolve:{
				isLogged:function ($location,$q,auth) {
				    var deferred = $q.defer();
				    if (auth.isLogged()) {
				        deferred.resolve();
				    } else {
				        deferred.reject();
				        $location.url('/login');
				    }
				    return deferred.promise;
				}
			},
		})
		.when ("/user/action", {
			templateUrl: "templates/action.html",
			resolve:{
				isLogged:function ($location,$q,auth) {
				    var deferred = $q.defer();
				    if (auth.isLogged()) {
				        deferred.resolve();
				    } else {
				        deferred.reject();
				        $location.url('/login');
				    }
				    return deferred.promise;
				}
			},
			//controller: "actionCtrl"
		})
		.when ("/user/recipe_builder", {
			templateUrl: "templates/recipe_builder.html",
			controller: "recipeBuilderCtrl",
			resolve:{
				isLogged:function ($location,$q,auth) {
				    var deferred = $q.defer();
				    if (auth.isLogged()) {
				        deferred.resolve();
				    } else {
				        deferred.reject();
				        $location.url('/login');
				    }
				    return deferred.promise;
				}
			},
		})
		.otherwise ({redirectTo: "/login"});
}]);
