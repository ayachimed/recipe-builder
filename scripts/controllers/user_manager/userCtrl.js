app.controller("userCtrl",["$scope","UserAccount","$rootScope",function ($scope,UserAccount,$rootScope) {
	$scope.tmp_acc = {};
	$scope.newUser={};
	UserAccount.getLevels(function (d) {
		if(d.success){
			$scope.levels = d.levels;
		}else{
			alert("error retrieving user levels");
		}
	});
	UserAccount.getUsers($rootScope.globals.user,function (data) {
		if(data.success){
			$scope.accounts_ready = true;
			$scope.accounts = data.accounts;
		}else{
			alert('error retrieving all users');
		}
	});

	$scope.addUser = function () {
		$scope.newUser.enabled = false;
		UserAccount.addUser($scope.newUser,function (d) {
			if(d.success){
				$scope.newUser.enabled = true;
				$scope.accounts.push($scope.newUser);
				$scope.newUser = {};
				$scope.form.$setPristine();
				$scope.form.$setUntouched();
				$('#newUserModal').modal("hide");
			}else{
				alert('error add');
			}
		});
	}
	$scope.delete_user=function (index) {
		UserAccount.deleteUser($scope.accounts[index],function (d) {
			if(d.success){
				$scope.accounts[index].deleted=true;
			}else{
				alert('error occured while deleting user');
			}
		});
	}
	$scope.edit_user = function (index) {
		$scope.tmp_acc = $scope.cloneObject($scope.accounts[index]);
		$scope.tmp_acc.index = index;
		$('#editModal').modal("show");
	}
	$scope.submit_edit_user = function () {
		UserAccount.updateUser($scope.tmp_acc,function (d) {
			if(d.success){
				$scope.accounts[$scope.tmp_acc.index] = $scope.tmp_acc;
				$('#editModal').modal("hide");	
			}else{
				alert('error occured while updating user');
			}
		});
	}
	$scope.cloneObject = function(obj) {
	    if (obj === null || typeof obj !== 'object') {
	        return obj;
	    }
	 
	    var temp = obj.constructor(); // give temp the original obj's constructor
	    for (var key in obj) {
	        temp[key] = $scope.cloneObject(obj[key]);
	    }
	    return temp;
	}

	$('#newUserModal').on("hidden.bs.modal", function () {
		$scope.form.$setPristine();
		$scope.form.$setUntouched();
	});
}]);