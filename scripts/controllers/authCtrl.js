app.controller("authCtrl",["$scope","auth","$location","$rootScope",function ($scope,auth,$location,$rootScope) {
	$scope.employee={};
	$scope.logged = false;
	$scope.login = function () {
		$scope.logged = true;
		auth.login($scope.employee,function (data) {
			if(data.success){
				$scope.logged = false;
				$rootScope.globals = {
					user:data.user
				}
				$location.path('/user/action')
			}else{
				$scope.error_login = true;
				$scope.errorMessage = data.message;
				$scope.employee = {};
				$scope.form.$setPristine();
				$scope.form.$setUntouched();
			}
		})
	}
}]);