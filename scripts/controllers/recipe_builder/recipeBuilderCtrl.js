app.controller("recipeBuilderCtrl",["$scope","Util",function ($scope,Util) {
	$scope.ingredientIndex = 0;

	//later this array will be filled via $http
	$scope.ingredients = Util.getIngredients();
	$scope.recipeProcess=[];

	$scope.ingredientsOptions={
		sort: false,
		group:{
		  animation: 100,
		  pull: "clone",
		},
		onStart:function (e) {
		}
	}

	$scope.processOptions={
		group: {
		    sort:true,
		    put: function (to) {
		      return true;
		    },
		    pull:function () {
		      return true;
		    }
		  },
		  animation: 100,
		  onMove:function (e) {
		    document.getElementsByClassName('trash')[0].style.display = "block";
		  },
		  onAdd: function (evt) {
		    var item = evt.item;  // dragged HTMLElement
		    $scope.recipeProcess.push(Util.cloneObject($scope.ingredients[item.getAttribute("index")]));
		    item.setAttribute("index",$scope.recipeProcess.length-1);
		    Util.doProperties (item,$scope);
		    item.addEventListener ("mousedown", function(e) {
		      Util.doProperties(item,$scope);
		    });
		  },
		  onUpdate:function (evt) {
    		Util.doProperties (evt.item,$scope);
		  },
		  onEnd:function () {
		    document.getElementsByClassName('trash')[0].style.display = "none";
		  },
	}

	$scope.trash={
		    group: {
		      sort:true,
		      put: function (to) {
		        return true;
		      },
		    },
		    animation: 100,
		    onAdd:function (event) {
		      $scope.recipeProcess.splice(event.item.getAttribute("index"),1);
		      var elements = document.getElementById('left').getElementsByClassName('element');
		      var index = event.item.getAttribute("index");
		      for(var i = 0;i<$scope.recipeProcess.length;++i){
		          if(elements[i].getAttribute("index") > index){
		            elements[i].setAttribute("index",elements[i].getAttribute("index")-1);
		          }
		      }
		      document.getElementsByClassName('trash')[0].style.display = "none";
		      document.getElementsByClassName('trash')[0].innerHTML = "";
		    }
	}
}]);
